﻿import Greeter1 = require("./Scripts/Greeter1");
import Greeter = require("./../../../Codebase/Greeter");

let greeter1 = new Greeter1(document.getElementById('greeter1'));
greeter1.start();

let greeter = new Greeter(document.getElementById('greeter'));
greeter.start();
