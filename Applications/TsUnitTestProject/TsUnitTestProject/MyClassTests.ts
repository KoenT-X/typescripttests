﻿import * as tsUnit from 'tsUnit';
import * as MyModule from 'MyClass';

// This means you can just use MyClass in your code, rather than the full MyModule.MyClass.
import MyClass = MyModule.MyClass;

export class MyClassTests extends tsUnit.TestClass 
{
    someTest() 
    {
        var v = new MyClass(1, 2);
        this.areIdentical(v.getSum(), 3);
    }

    someFailingTest()
    {
        var v = new MyClass(1, 2);
        this.areNotIdentical(v.getSum(), 3);
    }
}
