﻿import ClassB from "ClassB";

export class ClassA
{
    doAction()
    {
        console.log("Logs from ClassA.doAction:");
        console.log("ClassA parameter name  = " + ClassA.ParameterNames.SomeName);
        console.log("ClassB parameter name  = " + ClassB.ParameterNames.SomeName);
    }

    // This also works:
    //static ParameterNames =
    //{
    //    SomeName: "ClassA string"
    //}
}

export module ClassA
{
    export class ParameterNames
    {
        static SomeName = "ClassA string";
    }
}

export default ClassA;
