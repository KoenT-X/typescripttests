﻿import ClassA from "ClassA";

export class ClassB
{
    doAction()
    {
        console.log("Logs from ClassB.doAction:");
        console.log("ClassA parameter name  = " + ClassA.ParameterNames.SomeName);
        console.log("ClassB parameter name  = " + ClassB.ParameterNames.SomeName);
    }

    // This also works:
    //static ParameterNames =
    //{
    //    SomeName: "ClassB string"
    //}
}

export module ClassB
{
    export class ParameterNames
    {
        static SomeName = "ClassB string";
    }
}

export default ClassB;
