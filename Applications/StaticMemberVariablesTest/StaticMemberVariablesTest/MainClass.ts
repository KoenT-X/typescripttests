﻿import ClassA = require("ClassA");
import ClassB = require("ClassB");

class MainClass
{
    constructor()
    {
        console.log("In doSomething.");

        console.log("Logs from MainClass:");
        console.log("ClassA parameter name  = " + ClassA.ParameterNames.SomeName);
        console.log("ClassB parameter name  = " + ClassB.ParameterNames.SomeName);

        let objectA = new ClassA();
        let objectB = new ClassB();
        objectA.doNormalAction();
        objectB.doNormalAction(); // exception in here

        ClassA.doStaticAction();
        ClassB.doStaticAction();
    }
}

export = MainClass;
