﻿import ClassA = require("ClassA");

class ClassB
{
    doNormalAction()
    {
        console.log("Logs from ClassB.doNormalAction:");
        console.log("ClassA parameter name  = " + ClassA.ParameterNames.SomeName); // exception here: JavaScript runtime error: "Unable to get property 'SomeName' of undefined or null reference" (ClassA.ParameterNames is undefined)
        console.log("ClassB parameter name  = " + ClassB.ParameterNames.SomeName);
    }

    static doStaticAction()
    {
        console.log("Logs from ClassB.doStaticAction:");
        console.log("ClassA parameter name  = " + ClassA.ParameterNames.SomeName);
        console.log("ClassB parameter name  = " + ClassB.ParameterNames.SomeName);
    }

    //static ParameterNames =
    //{
    //    SomeName: "ClassB string"
    //}
}

module ClassB
{
    export class ParameterNames
    {
        static SomeName = "ClassB string";
    }

    //export module ParameterNames
    //{
    //    export const SomeName = "ClassB string";
    //}

    //export namespace ParameterNames
    //{
    //    export const SomeName = "ClassB string";
    //}

    //export class ParameterNames
    //{
    //    static get SomeName() { return "ClassB string"; }
    //}
}

export = ClassB;
