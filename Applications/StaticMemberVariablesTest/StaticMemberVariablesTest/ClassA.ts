﻿import ClassB = require("ClassB");

class ClassA
{
    doNormalAction()
    {
        console.log("Logs from ClassA.doNormalAction:");
        console.log("ClassA parameter name  = " + ClassA.ParameterNames.SomeName);
        console.log("ClassB parameter name  = " + ClassB.ParameterNames.SomeName);
    }

    static doStaticAction()
    {
        console.log("Logs from ClassA.doStaticAction:");
        console.log("ClassA parameter name  = " + ClassA.ParameterNames.SomeName);
        console.log("ClassB parameter name  = " + ClassB.ParameterNames.SomeName);
    }

    //static ParameterNames =
    //{
    //    SomeName: "ClassA string"
    //}
}

module ClassA
{
    export class ParameterNames
    {
        static SomeName = "ClassA string";
    }

    //export module ParameterNames
    //{
    //    export const SomeName = "ClassA string";
    //}

    //export namespace ParameterNames
    //{
    //    export const SomeName = "ClassA string";
    //}

    //export class ParameterNames
    //{
    //    static get SomeName() { return "ClassA string"; }
    //}
}

export = ClassA;
