﻿/// <reference path="ClassB" />

module MyModule
{
    export class ClassA
    {
        doAction()
        {
            console.log("Logs from ClassA.doAction:");
            console.log("ClassA parameter name  = " + ClassA.ParameterNames.SomeName);
            console.log("ClassB parameter name  = " + ClassB.ParameterNames.SomeName);
        }

        static ParameterNames =
        {
            SomeName: "ClassA string"
        }
    }
}
