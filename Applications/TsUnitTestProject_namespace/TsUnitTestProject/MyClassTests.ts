﻿/// <reference="references.ts" />

namespace MyCompany
{
    export class MyClassTests extends TsUnit.TestClass
    {
        someTest()
        {
            var v = new MyClass(1, 2);
            this.areIdentical(v.getSum(), 3);
        }
    }
}
