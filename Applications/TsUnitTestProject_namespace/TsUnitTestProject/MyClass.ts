﻿/// <reference="references.ts" />

namespace MyCompany
{
    export class MyClass
    {
        constructor(num1: number, num2: number)
        {
            this.num1_ = num1;
            this.num2_ = num2;
        }

        getSum(): number
        {
            return this.num1_ + this.num2_;
        }

        private num1_: number;
        private num2_: number;
    }
}
