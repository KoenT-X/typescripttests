﻿import { ClassA } from "./ClassA";
import { ClassB } from "./ClassB";

export class MainClass
{
    constructor()
    {
        console.log("Logs from MainClass:");
        console.log("ClassA parameter name  = " + ClassA.ParameterNames.SomeName);
        console.log("ClassB parameter name  = " + ClassB.ParameterNames.SomeName);

        let objectA = new ClassA();
        let objectB = new ClassB();
        objectA.doAction();
        objectB.doAction(); // exception in here
    }
}
