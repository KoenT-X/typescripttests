﻿import { ClassA } from "./ClassA";

export class ClassB
{
    doAction()
    {
        console.log("Logs from ClassB.doAction:");
        console.log("ClassA parameter name  = " + ClassA.ParameterNames.SomeName); // exception here: JavaScript runtime error: "Unable to get property 'SomeName' of undefined or null reference" (ClassA.ParameterNames is undefined)
        console.log("ClassB parameter name  = " + ClassB.ParameterNames.SomeName);
    }

    static ParameterNames =
    {
        SomeName: "ClassB string"
    }
}
