﻿import Greeter2 = require("Scripts/Greeter2");
import Greeter = require("./../../../Codebase/Greeter");

let greeter2 = new Greeter2(document.getElementById('greeter2'));
greeter2.start();

let greeter = new Greeter(document.getElementById('greeter'));
greeter.start();
